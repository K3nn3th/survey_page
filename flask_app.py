# A very simple Flask Hello World app for you to get started with...

from flask import Flask, request, flash, url_for, redirect, render_template, make_response, abort
import requests
import git
import csv, json
import os

GIT_TOKEN = 'UltraSecretToken'

app = Flask(__name__)
filename = "participant_data"

@app.route('/submit', methods=['POST'])
def submit():
    print('submitting data to university_records.csv')
    # name of csv file 
    data = request.form.values()
    with open(filename, 'a+') as data_file: 
        data_file.write(json.dumps(request.form) + '\n')    
    return render_template('submitted.html')

@app.route('/stats')
def stats():
    #riddims = os.listdir(os.getcwd()+'/survey_page/static/riddims/')
    data = ""
    with open(filename, 'r') as data_file: 
        json_data = data_file.readlines()

    html_string = "<div><b>" + str(len(json_data)) + " participants so far.</b></div><br><br>"
    for json in json_data:
        html_string += '<div>' + json + '</dev>'#render_template('survey.html', riddims=riddims)
    return html_string

@app.route('/')
def main_page():
    riddims = os.listdir(os.getcwd()+'/survey_page/static/riddims/')
    return render_template('survey.html', riddims=riddims)


@app.route('/accordion')
def accordion():
    return render_template('survey_accordion.html')

@app.route('/git_webhook', methods=['POST'])
def git_hook():
    print('___git called in!')
    if request.method == 'POST':

        # Do some checking first

        abort_code = 418
        # Do initial validations on required headers

        x_hub_signature = request.headers.get('X-Gitlab-Token')
        # webhook content type should be application/json for request.data to have the payload
        # request.data is empty in case of x-www-form-urlencoded
        if not x_hub_signature == GIT_TOKEN:
            print('Deploy signature failed: {sig}'.format(sig=x_hub_signature))
            abort(abort_code)

        payload = request.get_json()
        if payload is None:
            print('Deploy payload is empty: {payload}'.format(
                payload=payload))
            abort(abort_code)

        # Then do local git stuff
        repo = git.Repo('./survey_page')
        origin = repo.remotes.origin
        repo.create_head('main',
    origin.refs.main).set_tracking_branch(origin.refs.main).checkout()
        origin.pull()
        return '', 200
    else:
        return '', 400

if __name__ == '__main__':
    app.run(debug=True)
